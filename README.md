# PEC4

Este proyecto es un juego de plataformas en 2D en el que el jugador deberá avanzar esquivando enemigos y saltando plataformas hasta llegar a su objetivo.
El personaje puede moverse por el escenario, mediante las siguientes teclas: 'A' para desplazarse a la izquierda y 'D' para desplazarse a la derecha.
También se puede saltar pulsando la tecla del espacio.

Además, este juego tiene tres niveles de dificultad, en la que variará la fuerza del salto del personaje, la velocidad de movimiento de los enemigos y la frecuencia en la que caen las bolas de fuego.
El personaje solo tiene una vida y en cuanto resulta dañado se acabará la partida.

El juego dispone de cinco escenas, cada una con sus propia música: CompanyLogo (pantalla del logo de la empresa del juego), MainMenu (el menú principal), Level (el nivel del juego), Credits (pantalla de créditos), GameOver (la escena cuando pierde el jugador): 

- La escena de **CompanyLogo** contiene el logo de la empresa del juego mientras suena una música de fondo durante 7 segundos para después pasar automáticamente a la escena del menú pprincipal. Contiene los siguientes componentes:

    1. **LogoManager:** mediante el script LogoManager llama a la escena del menú principal a los 7 segundos.
    
    2. **AudioManager:** mediante los scripts AudioManager y Sound se realiza un control de toda la música y los efectos de sonidos del juego, por lo que se guarda como un Prefab y se utiliza en el resto de escenas.
    
    3. **GameSceneManager:** con el script GameSceneManager se permite realizar la llamada entre escena, guardándose también como un Prefab para utilizarse en el resto de escenas del juego.
    
- La escena de **MainMenu** es el menú principal del juego, el cual contiene tres botones, Enter, Credits y Exit. Cuando se pulsa al botón Enter, aparece un texto indicando que se seleccione la dificultad del juego, habiendo tres niveles de dificultad.
  Como se ha detallado al principio, estos niveles de dificultad varían tres factores. Por una parte, a mayor dificultad, el personaje tendrá una fuerza de salto menor, por lo que será más complicado el realizar los saltos entre las plataformas y esquivar a los enemigos, ya que se deberá hacer un salto más preciso. Además, los zombies aumentarán la velocidad a mayor nivel de dificultad. Por otra parte, el aumento de dificultad también provoca que la frecuencia en la que caen las bolas de fuego al final del juego sea más alta, dando menos oportunidad de esquivar al jugador.
  Además de poder elegir la dificultad, el botón de Credits permite ir a la ecesna de los Créditos para ver el nombre de las personas que han realizado el juego.
  Por último, también se encuentra el botón Exit para salir del juego. Esta escena contiene los siguientes componentes:

    1. **DifficultyManager:** el script DifficultyManager otorga los distintos valores que se dan mediante un PlayerPrefs dependiendo de la dificultad que se escoja.
    
    2. **AudioManager:** mediante los scripts AudioManager y Sound se realiza un control de toda la música y los efectos de sonidos del juego, por lo que se guarda como un Prefab y se utiliza en el resto de escenas.
    
    3. **GameSceneManager:** con el script GameSceneManager se permite realizar la llamada entre escena, guardándose también como un Prefab para utilizarse en el resto de escenas del juego.
    
- La escena **Level** contiene el nivel donde se desarrolla el juego. Se encuentra tanto el escenario, como el personaje y los distintos elementos que pueden provocar que jugador pierda la partida, como zombies o bolas de fuego. El nivel se compone de una sección laberíntica en la que si el jugador va en la dirección incorrecta, puede caer en un agujero con fuego. También en el camino hay numerosos zombies que se levantan en cuanto notan la presencia del jugador. Una vez superado el primer pasillo con zombies, el jugador debe de descender saltando entre plataformas mientras una roca va cayendo a sus espaldas. Una vez superada  esta parte, se encuentra otro pasillo lleno de zombies que en cuanto se logre pasar, se cerrará una puerta detrás del jugador. En ese cuarto cerrado se encuentra el tesoro necesario para pasar el juego. En cuanto el jugador coge la moneda, comienzan a caer bolas de fuego del techo y una vez finalicen de caer, aparecerá una cruz que en cuanto la toque, el personaje se teletransporta al principio del nivel, fuera de la cueva y finalizando el juego.
  Esta escena se compone de los siguientes componentes:

    1. **GameManager:** mediante el script GameManager se controla tanto la música principal de la escena como el paso a la escena de GameOver cuando el jugador pierde la partida o a la escena Credits cuando el jugador completa el juego.
    
    2. **AudioManager:** mediante los scripts AudioManager y Sound se realiza un control de toda la música y los efectos de sonidos del juego, por lo que se guarda como un Prefab y se utiliza en el resto de escenas.
    
    3. **GameSceneManager:** con el script GameSceneManager se permite realizar la llamada entre escena, guardándose también como un Prefab para utilizarse en el resto de escenas del juego.
    
    4. **Player:** en el script PlayerController se controla tanto el movimiento como el salto del jugador, además de activar tanto las puertas, como la roca o las bolas de fuego en cuanto atraviesa una zona o choca con un elemento en específico. También activa un sistema de partículas simulando sangre en cuanto un elemento daña al jugador.
    
    5. **Zombie:** el script ZombieController se encarga de activar la animación y movimiento del zombie en cuanto se encuentra cerca del jugador. Una vez comienza a moverse, no parará hasta que finalice la partida.
    
    6. **Coin:** el script CoinManager es el que inicializa la caída de bolas de fuego del techo en cuanto el jugador toca la moneda.
    
    7. **FireballSpawner:** mediante el script FireballSpawner se controla la frecuencia en la que caen las bolas de fuego del techo. 
    
- La escena **GameOver** tiene lugar cuando el jugador pierde la partida. Al jugador se le ofrecen las opciones de volver a jugar la partida en el mismo nivel de dificultad que había escogido anteriormente o volver al menú principal del juego. Esta escena contiene los siguientes componentes:

    1. **GameOverManager:** el script GameOverManager se encarga de ejecutar la música principal de la escena.
    
    2. **AudioManager:** mediante los scripts AudioManager y Sound se realiza un control de toda la música y los efectos de sonidos del juego, por lo que se guarda como un Prefab y se utiliza en el resto de escenas.
    
    3. **GameSceneManager:** con el script GameSceneManager se permite realizar la llamada entre escena, guardándose también como un Prefab para utilizarse en el resto de escenas del juego.
    
- La escena **Credits** aparece cuando el jugador completa el nivel o si se selecciona el botón de créditos en el menú principal del juego. Proporciona la información del equipo involucrado en el desarrollo del juego. Contiene los siguientes componentes:

    1. **CreditsManager:** el script CreditsManager se encarga de la música principal de la escena, además de cambiar a la escena del menú principal en cuanto pasan seis segundos.
    
    2. **AudioManager:** mediante los scripts AudioManager y Sound se realiza un control de toda la música y los efectos de sonidos del juego, por lo que se guarda como un Prefab y se utiliza en el resto de escenas.
    
    3. **GameSceneManager:** con el script GameSceneManager se permite realizar la llamada entre escena, guardándose también como un Prefab para utilizarse en el resto de escenas del juego.
    