﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    public Sound fireBallSound;

    private void Start()
    {
        PlayerPrefs.SetInt("InitSpawnFireball", 0);
        PlayerPrefs.SetInt("FreezeCameraY", 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            AudioManager.Instance.PlaySound(fireBallSound);
            PlayerPrefs.SetInt("InitSpawnFireball", 1);
            PlayerPrefs.SetInt("FreezeCameraY", 1);
            Destroy(this.gameObject);
        }
    }
}
