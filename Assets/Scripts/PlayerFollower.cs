﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour
{
    public Transform Player;
    private Vector3 offset;
    private float positionYPlayer;

    private void Update()
    {
        if (PlayerPrefs.GetInt("FreezeCameraY") == 1)
        {
            transform.position = new Vector3(Player.position.x, positionYPlayer, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(Player.position.x, Player.position.y, transform.position.z);
            positionYPlayer = Player.position.y + 2.5f;
        }
    }
}
