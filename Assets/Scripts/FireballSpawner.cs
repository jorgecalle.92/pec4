﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballSpawner : MonoBehaviour
{
    public GameObject coin;
    public GameObject fireball;
    public GameObject gate;

    private int counterFireball= 0;

    public void Update()
    {
        if(PlayerPrefs.GetInt("InitSpawnFireball") == 1)
        {
            PlayerPrefs.SetInt("InitSpawnFireball", 0);
            InvokeRepeating("SpawnFireball", 0.5f, PlayerPrefs.GetFloat("FireballRepeatRate"));
        }
    }

    public void SpawnFireball()
    {
        if(counterFireball<20)
        {
            Instantiate(fireball, new Vector2(this.transform.position.x - Random.Range(-4, 12),
            this.transform.position.y), Quaternion.identity);

            counterFireball++;
        }
        else
        {
            PlayerPrefs.SetInt("FreezeCameraY", 0);
            gate.SetActive(true);
        }
    }
}
