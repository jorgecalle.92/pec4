﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameSceneManager sceneManager;
    public Sound levelSound;

    private void Awake()
    {
        sceneManager = new GameSceneManager();
        PlayerPrefs.SetInt("PlayerIsDead", 0);
        PlayerPrefs.SetInt("PlayerWin", 0);
    }

    private void Start()
    {
        AudioManager.Instance.PlaySound(levelSound);
    }

    private void Update()
    {
        if (PlayerPrefs.GetInt("PlayerIsDead") == 1)
        {
            Invoke("PlayerDead", 4);
        }

        if (PlayerPrefs.GetInt("PlayerWin") == 1)
        {
            Invoke("CreditsScene", 8);
        }
    }

    private void PlayerDead()
    {
        sceneManager.LoadScene("GameOver");
    }

    private void CreditsScene()
    {
        sceneManager.LoadScene("Credits");
    }
}
