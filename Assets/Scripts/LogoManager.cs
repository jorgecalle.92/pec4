﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoManager : MonoBehaviour
{
    public Sound logoSound;

    private GameSceneManager sceneManager;

    private void Awake()
    {
        sceneManager = new GameSceneManager();
    }

    private void Start()
    {
        AudioManager.Instance.PlaySound(logoSound);
    }

    private void Update()
    {
        Invoke("MenuScene", 7);
    }

    private void MenuScene()
    {
        sceneManager.LoadScene("MainMenu");
    }
}
