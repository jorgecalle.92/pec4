﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 7;
    private float jumpPower;
    public Transform BottomPlayer;
    public LayerMask groundMask;
    public GameObject BloodParticle;
    public GameObject InitialBlock;
    public GameObject Rock;
    public GameObject MediumBlock;
    public Sound deadSound;
    public Sound zombieSound;
    public Sound rockSound;
    public Sound gateSound;

    private Rigidbody2D rb;
    private float playerRadius = 0.07f;
    private bool onGround = true;
    private bool isRight = true;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        PlayerPrefs.SetInt("PlayerIsJump", 1);
        PlayerPrefs.SetInt("PlayerIsGround", 1);
        jumpPower = PlayerPrefs.GetFloat("PlayerJumpPower");
    }

    void FixedUpdate()
    {
        onGround = Physics2D.OverlapCircle(BottomPlayer.position, playerRadius, groundMask);

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            if (isRight)
            {
                isRight = false;
                rb.transform.Rotate(0, 180, 0);
            }

            Move();
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            if (!isRight)
            {
                isRight = true;
                rb.transform.Rotate(0, 180, 0);
            }

            Move();
        }

        if (Input.GetKey(KeyCode.Space) && onGround)
        {
            PlayerPrefs.SetInt("PlayerIsGround", 0);
            Jump();
        }

        if (onGround)
        {
            PlayerPrefs.SetInt("PlayerIsGround", 1);
        }
        else
        {
            PlayerPrefs.SetInt("PlayerIsGround", 0);
        }

        PlayerPrefs.SetFloat("PlayerVelocity", Input.GetAxis("Horizontal"));
    }

    private void Move()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        rb.transform.position += movement * Time.deltaTime * moveSpeed;
    }

    private void Jump()
    {
        PlayerPrefs.SetInt("PlayerIsJump", 1);
        rb.velocity = new Vector2(rb.velocity.x, jumpPower);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Zombie") || collision.gameObject.CompareTag("Fire") ||
            collision.gameObject.CompareTag("Rock") || collision.gameObject.CompareTag("Fireball"))
        {
            AudioManager.Instance.PlaySound(deadSound);
            Instantiate(BloodParticle, transform.position, Quaternion.identity);
            PlayerPrefs.SetInt("PlayerIsDead", 1);
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("InitialStatue"))
        {
            AudioManager.Instance.PlaySound(zombieSound);
            InitialBlock.SetActive(true);
        }

        if(collision.CompareTag("ActiveRock"))
        {
            AudioManager.Instance.PlaySound(rockSound);
            Rock.SetActive(true);
        }

        if(collision.CompareTag("InitialMediumBlock"))
        {
            MediumBlock.SetActive(true);
        }

        if (collision.gameObject.CompareTag("Gate"))
        {
            AudioManager.Instance.PlaySound(gateSound);
            rb.transform.position = new Vector3(-7.91f, -2.4f, 0);
            PlayerPrefs.SetInt("PlayerWin", 1);
        }
    }
}
