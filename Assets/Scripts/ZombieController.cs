﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    public bool rightDirection = false;

    private float moveSpeed;
    private Rigidbody2D rb;
    private Vector3 movement;
    private bool startMove = false;
    private Animator animatorZombie;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animatorZombie = GetComponent<Animator>();
        moveSpeed= PlayerPrefs.GetFloat("ZombieSpeed");

        if(rightDirection)
        {
            moveSpeed *= -1;
        }
    }
    
    void Update()
    {
        //Se mueve en una única dirección, y cuando choca con algo, cambia su dirección y rotación.
        movement = new Vector3(-1, 0f, 0f);

        //Justo un segundo antes de que la cámara enfoque al enemigo,inicia su movimiento
        if (rb.transform.position.x - GameObject.FindGameObjectWithTag("MainCamera").transform.position.x < 10
                && GameObject.FindGameObjectWithTag("MainCamera").transform.position.y - rb.transform.position.y  < 1)
        {
            animatorZombie.SetBool("PlayerIsNear", true);
            Invoke("ZombieResurrect", 0.5f);
        }
        
        if (startMove)
        {
            Move();
        }
    }

    private void ZombieResurrect()
    {
        animatorZombie.SetBool("ZombieResurrect", true);
        startMove = true;
    }

    private void Move()
    {
        rb.transform.position += movement * Time.deltaTime * moveSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacle") || collision.gameObject.CompareTag("Zombie"))
        {
            moveSpeed *= -1;
            rb.transform.Rotate(0, 180, 0);
        }
    }
}
