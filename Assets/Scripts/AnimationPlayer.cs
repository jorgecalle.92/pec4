﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour
{
    private Rigidbody2D rigidbodyPlayer;
    private Animator animatorPlayer;

    void Awake()
    {
        rigidbodyPlayer = GetComponent<Rigidbody2D>();
        animatorPlayer = GetComponent<Animator>();
    }

    void Update()
    {
        if (PlayerPrefs.GetInt("PlayerIsJump") == 1 && PlayerPrefs.GetInt("PlayerIsGround") == 0)
        {
            animatorPlayer.SetBool("PlayerIsJump", true);
            animatorPlayer.SetBool("PlayerIsGround", false);
            animatorPlayer.SetFloat("PlayerVelocity", 0);
        }
        else
        {
            animatorPlayer.SetBool("PlayerIsJump", false);
            animatorPlayer.SetBool("PlayerIsGround", true);

            //Si se mueve a la izquierda, se cambia el signo para que se pueda seguir con la comprobación de si es mayor que 0.1
            if (PlayerPrefs.GetFloat("PlayerVelocity") < 0)
            {
                animatorPlayer.SetFloat("PlayerVelocity", -PlayerPrefs.GetFloat("PlayerVelocity"));
            }
            else
            {
                animatorPlayer.SetFloat("PlayerVelocity", PlayerPrefs.GetFloat("PlayerVelocity"));
            }
        }
    }
}
