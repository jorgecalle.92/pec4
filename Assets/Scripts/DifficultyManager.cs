﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManager : MonoBehaviour
{
    public GameObject ButtonEnter;
    public GameObject ButtonCredits;
    public GameObject ButtonExit;
    public GameObject TextDifficulty;
    public GameObject ButtonEasy;
    public GameObject ButtonNormal;
    public GameObject ButtonNightmare;
    public Sound mainMenuSound;

    private void Start()
    {
        AudioManager.Instance.PlaySound(mainMenuSound);
    }

    public void ActiveDifficulty()
    {
        ButtonEnter.SetActive(false);
        ButtonCredits.SetActive(false);
        ButtonExit.SetActive(false);
        TextDifficulty.SetActive(true);
        ButtonEasy.SetActive(true);
        ButtonNormal.SetActive(true);
        ButtonNightmare.SetActive(true);
    }

    public void Easy()
    {
        PlayerPrefs.SetFloat("PlayerJumpPower", 17);
        PlayerPrefs.SetFloat("ZombieSpeed", 3);
        PlayerPrefs.SetFloat("FireballRepeatRate", 0.5f);
    }

    public void Normal()
    {
        PlayerPrefs.SetFloat("PlayerJumpPower", 16.5f);
        PlayerPrefs.SetFloat("ZombieSpeed", 4);
        PlayerPrefs.SetFloat("FireballRepeatRate", 0.4f);
    }

    public void Nightmare()
    {
        PlayerPrefs.SetFloat("PlayerJumpPower", 16);
        PlayerPrefs.SetFloat("ZombieSpeed", 5);
        PlayerPrefs.SetFloat("FireballRepeatRate", 0.3f);
    }
}
