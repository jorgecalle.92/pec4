﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsManager : MonoBehaviour
{
    public Sound creditsSound;

    private GameSceneManager sceneManager;

    private void Awake()
    {
        sceneManager = new GameSceneManager();
    }

    private void Start()
    {
        AudioManager.Instance.PlaySound(creditsSound);
    }

    private void Update()
    {
        Invoke("MenuScene", 6);
    }

    private void MenuScene()
    {
        sceneManager.LoadScene("MainMenu");
    }
}
