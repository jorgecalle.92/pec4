﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public Sound gameOverSound;

    void Start()
    {
        AudioManager.Instance.PlaySound(gameOverSound);
    }
}
